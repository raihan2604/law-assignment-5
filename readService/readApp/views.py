from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import connection as conn

# Create your views here.
@api_view(['GET'])
def get_data_api(request, npm):
    cursor = conn.cursor()
    cursor.execute("SELECT * from update_app_datamodels WHERE npm='"+npm+"'")
    data = cursor.fetchall()
    print(data)

    return Response({
        'status': 'OK',
        'npm':data[0][0],
        'nama':data[0][1]
        })
        
@api_view(['GET'])
def get_data_api_cookie(request, npm, cookie):
    cursor = conn.cursor()
    cursor.execute("SELECT * from update_app_datamodels WHERE npm='"+npm+"'")
    data = cursor.fetchall()
    print(data)

    return Response({
        'status': 'OK',
        'npm':data[0][0],
        'nama':data[0][1]
        })