from rest_framework import  serializers
from .models import DataModels

class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataModels
        fields = '__all__'