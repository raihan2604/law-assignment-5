from rest_framework import generics
from rest_framework.response import Response
from .serializers import DataSerializer
from .models import DataModels
from rest_framework.decorators import api_view

@api_view(['POST'])
def DataCreateApi(request):
    data_arr = DataModels.objects.filter(npm=request.data['npm'])
    if data_arr.count()==0:
        data = DataModels.objects.create(
            npm=request.data['npm'],
            nama = request.data['nama']
        )
        data.save()
    else:
        data = data_arr.first()
        data.nama = request.data['nama']
        data.save()
    return Response({'Status':'OK'})